import React from 'react';
import { render } from '@testing-library/react';
import { TodoContainer } from '../TodoContainer';

describe('should test todo app', () => {
  it('should test the creation of the component', () => {
    const { container } = render(<TodoContainer />);
    expect(container).toBeTruthy();
  });
});
