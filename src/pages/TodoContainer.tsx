import React from 'react';
import TodoList from '../components/TodoList/TodoList';

export const TodoContainer = () => {
  return (
    <div>
      <TodoList />
    </div>
  );
};
