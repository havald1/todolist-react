import React from 'react';
import './App.css';
import { TodoContainer } from './pages/TodoContainer';

function App() {
  return (
    <div>
      <TodoContainer />
    </div>
  );
}

export default App;
