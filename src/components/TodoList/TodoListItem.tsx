import React from 'react';
import { Todo } from './types/Todo';
import './TodoListItem.css';

const TodoListItem = ({ todo, done }: TodoListItemProps) => {
  const changeDone = () => {
    done(todo);
  };

  let className = '';
  if (todo.done) {
    className = 'checked';
  }

  return (
    <>
      <li className={className} onClick={changeDone}>
        {todo.text}
      </li>
    </>
  );
};

interface TodoListItemProps {
  todo: Todo;
  done: (todo: Todo) => void;
}

export default TodoListItem;
