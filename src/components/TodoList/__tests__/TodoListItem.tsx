import React from 'react';
import { render } from '@testing-library/react';
import TodoListItem from '../TodoListItem';
import { Todo } from '../types/Todo';
import userEvent from '@testing-library/user-event';

const dummyTodo: Todo = {
  id: 0,
  text: 'someTodo',
  done: false,
};

describe('should test the logic of TodoListItem', () => {
  it('should test the creation of an TodoLitItem', () => {
    const { container } = render(
      <TodoListItem todo={dummyTodo} done={() => {}} />
    );
    expect(container).toBeTruthy();
  });

  it('should test if the callback to update the done state is called', () => {
    const doneSpy = jest.fn();
    const { container } = render(
      <TodoListItem todo={dummyTodo} done={doneSpy} />
    );
    userEvent.click(container.firstChild);
    expect(doneSpy).toHaveBeenCalledTimes(1);
  });
});

describe('Testing the visual part of the component', () => {
  it('should test if text is present', () => {
    const { getByText } = render(
      <TodoListItem todo={dummyTodo} done={() => {}} />
    );

    expect(getByText('someTodo')).toBeTruthy();
  });

  it('should test if the state is correctly represented', () => {
    const { container } = render(
      <TodoListItem todo={dummyTodo} done={() => {}} />
    );

    expect(container.firstChild).not.toHaveClass('checked');
  });

  it('should test if the state is correctly represented', () => {
    const doneTodo: Todo = { ...dummyTodo, done: true };
    const { container } = render(
      <TodoListItem todo={doneTodo} done={() => {}} />
    );

    expect(container.firstChild).toHaveClass('checked');
  });
});
