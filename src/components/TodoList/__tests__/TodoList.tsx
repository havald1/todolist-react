import React from 'react';

import { render } from '@testing-library/react';
import TodoList from '../TodoList';

describe('should test todo-list', () => {
  it('should test the creation of the component', () => {
    const { container } = render(<TodoList />);
    expect(container).toBeTruthy();
  });
});
