import React from 'react';
import { Todo } from './types/Todo';
import TodoListItem from './TodoListItem';
import { useState } from 'react';

const dummyData: Todo[] = [
  {
    id: 0,
    text: 'Todo1',
    done: false,
  },
  {
    id: 1,
    text: 'Todo2',
    done: false,
  },
  {
    id: 2,
    text: 'Todo3',
    done: true,
  },
];

const TodoList = () => {
  const [todos, setTodos] = useState(dummyData);

  const changeDone = (todo: Todo) => {
    const newTodos = todos.map((oldTodo: Todo) => {
      if (todo.id === oldTodo.id) {
        return { ...todo, done: !todo.done };
      }
      return oldTodo;
    });
    setTodos(newTodos);
  };

  return (
    <>
      <ul>
        {todos.map((todo: Todo) => (
          <TodoListItem todo={todo} key={todo.id} done={changeDone} />
        ))}
      </ul>
    </>
  );
};

export default TodoList;
